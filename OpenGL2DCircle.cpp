#include "OpenGL2DCircle.h"
#include "windows.h"
#include <gl\gl.h>
#include "Window/Window.h"

COpenGL2DCircle::COpenGL2DCircle()
	: CVisible()
	, m_fRadius( 1.f )
	, m_oPosition( 2 )
	, m_fRed( 1.f )
	, m_fGreen( 1.f )
	, m_fBlue( 1.f )
{
}

COpenGL2DCircle::COpenGL2DCircle( f32 fX, f32 fY, f32 fRadius, 
	f32 fRed, f32 fGreen, f32 fBlue )
	: CVisible()
	, m_fRadius( fRadius )
	, m_oPosition( fX, fY )
	, m_fRed( fRed )
	, m_fGreen( fGreen )
	, m_fBlue( fBlue )
{
}

void COpenGL2DCircle::Set( f32 fX, f32 fY, f32 fRadius, f32 fRed, f32 fGreen, f32 fBlue )
{
	m_fRadius = fRadius;
	m_oPosition.XY( fX, fY );
	m_fRed = fRed;
	m_fGreen = fGreen;
	m_fBlue = fBlue;
}

void COpenGL2DCircle::Draw() const
{	
	f32 fWindowHalfWidth = CWindow::GetDefaultWindow()->GetVirtualWidth() / 2;
	f32 fWindowHalfHeight = CWindow::GetDefaultWindow()->GetVirtualHeight() / 2;

	// Convert -half width to half width to -1 to 1:
	f32 fX( NUtilities::ConvertCoordinateNoBounds( m_oPosition.X(), -fWindowHalfWidth, fWindowHalfWidth, -1.f, 1.f ) );
	f32 fY( NUtilities::ConvertCoordinateNoBounds( m_oPosition.Y(), -fWindowHalfHeight, fWindowHalfHeight, -1.f, 1.f ) );
	f32 fXRadius( NUtilities::ConvertCoordinateNoBounds( m_fRadius, -fWindowHalfWidth, fWindowHalfWidth, -1.f, 1.f ) );
	f32 fYRadius( NUtilities::ConvertCoordinateNoBounds( m_fRadius, -fWindowHalfHeight, fWindowHalfHeight, -1.f, 1.f ) );

	assert( CWindow::GetDefaultWindow() );
	glColor3f( m_fRed, m_fGreen, m_fBlue );
	glBegin( GL_TRIANGLE_FAN );
		glVertex2f( fX, fY );
		for( f32 fAngle( 0 ); fAngle < 2 * PI; fAngle += 0.1f )
		{
			glVertex2f( fX + static_cast< GLfloat >( sin( fAngle ) ) * fXRadius, 
				fY + ( static_cast< f32 >( cos( fAngle ) ) * fYRadius ) );
		}
		glVertex2f( fX, fY + ( fYRadius ) );
	glEnd();
}

const CF32Vector& COpenGL2DCircle::GetPositionConst() const
{
	assert( m_oPosition.GetNumDimensions() == 2 );
	return m_oPosition;
}

CF32Vector& COpenGL2DCircle::GetPosition()
{
	assert( m_oPosition.GetNumDimensions() == 2 );
	return m_oPosition;
}

void COpenGL2DCircle::SetColor( f32 fRed, f32 fGreen, f32 fBlue )
{
	m_fRed = fRed;
	m_fGreen = fGreen;
	m_fBlue = fBlue;
}
